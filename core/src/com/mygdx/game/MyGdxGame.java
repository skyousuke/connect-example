package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.mygdx.game.items.*;

public class MyGdxGame extends ApplicationAdapter {
    SpriteBatch batch;
    Player player;
    Door door;
    Array<Item> items = new Array<Item>();

    ConnectionUi connectionUi;
    ConnectionLog connectionLog;
    Stage stage;

    Array<Item> fromItems = new Array<Item>();
    Array<Item> toItems = new Array<Item>();
    ObjectMap<Item, ProperlyConnection> properlyConnections = new ObjectMap<Item, ProperlyConnection>();

    @Override
    public void create() {
        batch = new SpriteBatch();
        Assets.instance.init();

        player = new Player(new Vector2(100, 100));

        SolarCell solarCell = new SolarCell(new Vector2(300, 100));
        Battery battery = new Battery(new Vector2(700, 400));
        Inverter inverter = new Inverter(new Vector2(700, 100));
        ChargeController chargeController = new ChargeController(new Vector2(300, 400));
        door = new Door(new Vector2(500, 250));

        items.add(solarCell);
        items.add(battery);
        items.add(inverter);
        items.add(chargeController);
        items.add(door);

        connectionLog = new ConnectionLog(this);
        connectionUi = new ConnectionUi(this);

        connectionUi.setVisible(false);

        stage = new Stage();
        stage.addActor(connectionUi);
        stage.addActor(connectionLog);

        Gdx.input.setInputProcessor(stage);

        properlyConnections.put(solarCell, new ProperlyConnection(chargeController));
        properlyConnections.put(battery, new ProperlyConnection(chargeController));
        properlyConnections.put(chargeController, new ProperlyConnection(solarCell, battery, inverter));
        properlyConnections.put(inverter, new ProperlyConnection(chargeController, door));
        properlyConnections.put(door, new ProperlyConnection(inverter));
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        player.handleInput(Gdx.graphics.getDeltaTime(), items, connectionUi);
        stage.act();

        batch.begin();
        for (Item item : items)
            item.draw(batch);
        player.draw(batch);
        batch.end();

        stage.draw();
    }

    @Override
    public void dispose() {
        batch.dispose();
        Assets.instance.dispose();
    }

    public void onConnectionChange(Item item, Item otherItem, boolean connectMode) {
        if (connectMode) {
            fromItems.add(item);
            toItems.add(otherItem);
        } else {
            int connectionIndex = findConnectionIndex(item, otherItem);
            if (connectionIndex == -1)
                connectionIndex = findConnectionIndex(otherItem, item);

            if (connectionIndex != -1) {
                fromItems.removeIndex(connectionIndex);
                toItems.removeIndex(connectionIndex);
            }
        }
        connectionLog.update();
        properlyConnections.get(item).updateConnection(otherItem, connectMode);
        properlyConnections.get(otherItem).updateConnection(item, connectMode);
        door.setOpen(isProperlyConnected());
    }

    private boolean isProperlyConnected() {
        for (ProperlyConnection properlyConnection : properlyConnections.values()) {
            if (!properlyConnection.isSatisfied())
               return false;
        }
        return true;
    }

    private int findConnectionIndex(Item item, Item otherItem) {
        for (int i = 0; i < fromItems.size; i++) {
            if (fromItems.get(i) == item && toItems.get(i) == otherItem) {
                return i;
            }
        }
        return -1;
    }

    public boolean hasConnection(Item item, Item otherItem) {
        return item != otherItem && (findConnectionIndex(item, otherItem) != -1 || findConnectionIndex(otherItem, item) != -1);
    }
    public boolean noConnection(Item item, Item otherItem) {
        return item != otherItem && findConnectionIndex(item, otherItem) == -1 && findConnectionIndex(otherItem, item) == -1;
    }

    class ProperlyConnection {
        ObjectSet<Item> otherItems = new ObjectSet<Item>();
        ObjectSet<Item> currentOtherItems = new ObjectSet<Item>();

        ProperlyConnection(Item... otherItems) {
            this.otherItems.addAll(otherItems);
        }

        void updateConnection(Item otherItem, boolean connectMode) {
            if (connectMode)
                currentOtherItems.add(otherItem);
            else
                currentOtherItems.remove(otherItem);
        }

        boolean isSatisfied() {
            return otherItems.equals(currentOtherItems);
        }
    }
}
