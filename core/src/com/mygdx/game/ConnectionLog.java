package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.items.Item;

public class ConnectionLog extends Table {

    private Array<Item> printedLeft = new Array<Item>();
    private Array<Item> printedRight = new Array<Item>();

    private boolean duplicateMode;
    private TextButton toggleModeButton;

    private MyGdxGame game;

    public ConnectionLog(MyGdxGame game) {
        this.game = game;
        TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();

        TextureRegionDrawable whiteDrawable = new TextureRegionDrawable(new TextureRegion(Assets.instance.white));
        buttonStyle.up = whiteDrawable.tint(Color.LIGHT_GRAY);
        buttonStyle.down = whiteDrawable.tint(Color.GRAY);
        buttonStyle.over = whiteDrawable.tint(new Color(0.65f, 0.65f, 0.65f, 1f));
        buttonStyle.font = Assets.instance.font;
        buttonStyle.fontColor = Color.BLACK;

        toggleModeButton = new TextButton("Long Mode", buttonStyle);
        toggleModeButton.pad(0, 5, 0, 5);
        toggleModeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                duplicateMode = !duplicateMode;
                String text = duplicateMode ? "Short Mode" : "Long Mode";
                toggleModeButton.setText(text);
                update();
            }
        });
    }

    public void update() {
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = Assets.instance.font;
        labelStyle.fontColor = Color.WHITE;

        printedLeft.clear();
        printedRight.clear();
        clearChildren();
        row().left();
        add(new Label("Connection Log", labelStyle));
        add(toggleModeButton).padLeft(10).fillX();
        row().left();
        add(new Label("------------------", labelStyle)).colspan(2);

        for (int i = 0; i < game.fromItems.size; i++) {
            Item fromItem = game.fromItems.get(i);
            Item toItem = game.toItems.get(i);
            if (!isDuplicate(fromItem, toItem) || duplicateMode) {
                row().left();
                add(new Label(fromItem.getName() + " --- " + toItem.getName(), labelStyle)).colspan(2);
                printedLeft.add(fromItem);
                printedRight.add(toItem);
            }
        }
        pack();
        setPosition(900, 600, Align.topLeft);
    }

    private boolean isDuplicate(Item item, Item connectingItem) {
        for (int i = 0; i < printedLeft.size; i++) {
            Item leftItem = printedLeft.get(i);
            if (leftItem == connectingItem && printedRight.get(i) == item)
                return true;
        }
        return false;
    }
}
