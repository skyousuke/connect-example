package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Assets {
    public static final Assets instance = new Assets();

    public Texture solarCell;
    public Texture inverter;
    public Texture battery;
    public Texture chargeController;
    public Texture door;
    public Texture openedDoor;
    public Texture player;

    public Texture white;    
    
    public BitmapFont font;

    public void init() {
        solarCell = filledTexture(32, 32, Color.ROYAL);
        inverter = filledTexture(32, 32, Color.LIME);
        battery = filledTexture(32, 32, Color.ORANGE);
        chargeController = filledTexture(32, 32, Color.CYAN);
        door = filledTexture(32, 32, Color.PINK);
        openedDoor = filledTexture(32, 32, Color.YELLOW);
        player = filledTexture(32, 32, Color.SCARLET);
        white = filledTexture(1, 1, Color.WHITE);
        font = new BitmapFont();
    }

    public void dispose() {
        solarCell.dispose();
        inverter.dispose();
        battery.dispose();
        chargeController.dispose();
        door.dispose();
        player.dispose();
        
        font.dispose();
    }

    private Texture filledTexture(int width, int height, Color color) {
        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGB888);
        pixmap.setColor(color);
        pixmap.fill();
        return new Texture(pixmap);
    }
}
