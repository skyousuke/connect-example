package com.mygdx.game.items;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Assets;

public class Door extends Item {

    public Door(Vector2 position) {
        super(Assets.instance.door, position, "Door");
    }

    public void setOpen(boolean open) {
        if (open)
            texture = Assets.instance.openedDoor;
        else
            texture = Assets.instance.door;
    }
}
