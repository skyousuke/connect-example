package com.mygdx.game.items;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mygdx.game.Assets;
import com.mygdx.game.GameObject;


public abstract class Item extends GameObject {
    private String name;

    public Item(Texture texture, Vector2 position, String name) {
        super(texture);
        this.position.set(position);
        this.name = name;
    }

    @Override
    public void draw(SpriteBatch batch) {
        super.draw(batch);
        Assets.instance.font.draw(batch, name, position.x, position.y);
    }

    public Image getIcon() {
        return new Image(texture);
    }

    public String getName() {
        return name;
    }
}
