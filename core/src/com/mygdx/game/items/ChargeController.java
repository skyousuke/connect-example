package com.mygdx.game.items;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Assets;

public class ChargeController extends Item {

    public ChargeController(Vector2 position) {
        super(Assets.instance.chargeController, position, "ChargeController");
    }
}
