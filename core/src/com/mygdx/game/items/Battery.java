package com.mygdx.game.items;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Assets;

public class Battery extends Item {

    public Battery(Vector2 position) {
        super(Assets.instance.battery, position, "Battery");
    }
}
