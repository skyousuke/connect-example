package com.mygdx.game.items;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Assets;

public class Inverter extends Item {

    public Inverter(Vector2 position) {
        super(Assets.instance.inverter, position, "Inverter");
    }
}
