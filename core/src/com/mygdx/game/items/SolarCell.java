package com.mygdx.game.items;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Assets;

public class SolarCell extends Item {

    public SolarCell(Vector2 position) {
        super(Assets.instance.solarCell, position, "SolarCell");
    }
}
