package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.items.Item;

public class Player extends GameObject {

    public Player(Vector2 position) {
        super(Assets.instance.player);
        this.position.set(position);
    }

    public void handleInput(float delta, Array<Item> items, ConnectionUi connectionUi) {
        if (!connectionUi.isVisible()) {
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
                position.x -= 150 *delta;
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
                position.x += 150 *delta;
            if (Gdx.input.isKeyPressed(Input.Keys.DOWN))
                position.y -= 150 *delta;
            if (Gdx.input.isKeyPressed(Input.Keys.UP))
                position.y += 150 *delta;
            if (Gdx.input.isKeyJustPressed(Input.Keys.A))
                interactItem(items, connectionUi, true);
            else if (Gdx.input.isKeyJustPressed(Input.Keys.S))
                interactItem(items, connectionUi, false);
        } else {
            if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
                connectionUi.setVisible(false);
        }
    }

    private void interactItem(Array<Item> items, ConnectionUi connectionUi, boolean connectMode) {
        for (Item item : items) {
            Rectangle.tmp.set(item.position.x, item.position.y, 32, 32);
            Rectangle.tmp2.set(position.x, position.y, 32, 32);
            if (Rectangle.tmp.overlaps(Rectangle.tmp2)) {
                connectionUi.update(item, connectMode);
                connectionUi.setVisible(true);
                break;
            }
        }
    }
}
