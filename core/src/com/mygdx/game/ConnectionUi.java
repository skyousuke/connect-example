package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.items.Item;

public class ConnectionUi extends Table {
    private TextButton closeButton;
    private MyGdxGame game;

    public ConnectionUi(MyGdxGame game) {
        this.game = game;

        TextureRegionDrawable whiteDrawable = new TextureRegionDrawable(new TextureRegion(Assets.instance.white));
        setBackground(whiteDrawable);

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = whiteDrawable.tint(Color.LIGHT_GRAY);
        textButtonStyle.down = whiteDrawable.tint(Color.GRAY);
        textButtonStyle.over = whiteDrawable.tint(new Color(0.65f, 0.65f, 0.65f, 1f));
        textButtonStyle.font = Assets.instance.font;
        textButtonStyle.fontColor = Color.RED;

        closeButton = new TextButton("X", textButtonStyle);
        closeButton.pad(0, 5, 0, 5);
        closeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                setVisible(false);
            }
        });
    }

    public void update(final Item item, boolean connectMode) {
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = Assets.instance.font;
        labelStyle.fontColor = Color.BLACK;

        clearChildren();
        row();
        add(closeButton).right();
        pad(10, 20, 20, 20);
        row().padTop(10);
        add(item.getIcon());
        row().padTop(10);

        if (connectMode) {
            add(new Label("Connect To", labelStyle));
            row().padTop(10);
            add(createItemTable(item, true));
        }
        else {
            add(new Label("Disconnect From", labelStyle));
            row().padTop(10);
            add(createItemTable(item, false));
        }
        pack();
        setPosition(1280 / 2f, 600 / 2f, Align.center);
    }

    private Table createItemTable(final Item item, boolean connectMode) {
        Table imageTable = new Table();

        TextureRegionDrawable whiteDrawable = (TextureRegionDrawable) getBackground();
        ImageButton.ImageButtonStyle baseImageButtonStyle = new ImageButton.ImageButtonStyle();
        baseImageButtonStyle.up = whiteDrawable.tint(Color.LIGHT_GRAY);

        for (final Item otherItem : game.items) {
            if (connectMode) {
                if (game.noConnection(item, otherItem)) {
                    baseImageButtonStyle.down = whiteDrawable.tint(Color.GOLD);
                    baseImageButtonStyle.over = whiteDrawable.tint(Color.YELLOW);
                    imageTable.add(createButton(item, otherItem, true, baseImageButtonStyle)).size(48, 48).pad(0, 5, 0, 5);
                }
            }
            else {
                if (game.hasConnection(item, otherItem)) {
                    baseImageButtonStyle.down = whiteDrawable.tint(Color.SCARLET);
                    baseImageButtonStyle.over = whiteDrawable.tint(Color.RED);
                    imageTable.add(createButton(item, otherItem, false, baseImageButtonStyle)).size(48, 48).pad(0, 5, 0, 5);
                }
            }
        }
        return imageTable;
    }

    private ImageButton createButton(final Item item, final Item otherItem, final boolean connectMode,
                                     ImageButton.ImageButtonStyle baseImageButtonStyle) {
        ImageButton.ImageButtonStyle imageButtonStyle = new ImageButton.ImageButtonStyle(baseImageButtonStyle);
        imageButtonStyle.imageUp = otherItem.getIcon().getDrawable();
        ImageButton button = new ImageButton(imageButtonStyle);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                notifyConnectionChange(item, otherItem, connectMode);
                setVisible(false);
            }
        });
        return button;
    }

    private void notifyConnectionChange(Item item, Item otherItem, boolean connectMode) {
        game.connectionLog.update();
        game.onConnectionChange(item, otherItem, connectMode);
    }
}
